# CAN bootloader dla STM32F103

---

Projekt bootloadera dla mikrokontrolerów STM32. Ma on służyć jako szablon dla ogółu mikrokontrolerów tej rodziny, ale ten konkretny przykład zawiera wersję dla STM32F103C8T6, w który wyposażony jest tzw. bluepill. Oprócz samego bootloadera przeznaczonego do zainstalowania na samym mikrokontrolerze, repozytorium zawiera również skrypt napisany w pythonie, który ułatwia korzystanie z bootloadera.

#### Konfiguracja

---

Poniżej określono zastosowaną prędkość magistrali CAN, wykorzystane piny oraz zarezerwowaną pamięć. Oprócz tego, użyto też pinu PC13, pod który w bluepillu podpięta jest dioda led, która miga po odebraniu przez mikrokontroler ramki CAN - w celu sprawdzenia czy bootloader działa.

| Piny CAN   | Bitrate    | Pin LED | Rozmiar |
|------------|------------|---------|---------|
| PA11, PA12 | 125 kbit/s | PC13    | 8 kB    |

#### Działanie

---

Bootloader komunikuje się z programatorem korzystając z ramek CAN. Cała treść znajduje się w nagłówkach. Po dwóch sekundach po resecie mikrokontroler wysyła ramkę CAN, sygnalizując swoją obecność. Jeśli w ciągu kolejnej sekundy otrzyma od komputera ramkę zwrotną o odpowiednim nagłówku, przechodzi do pętli głównej bootloadera. Następnie sprawdza obecność zainstalowanego programu - w przypadku jego braku również przechodzi do pętli. Jeśli program zostanie wykryty, mikrokontroler skacze do niego.  
W pętli głównej bootloader czeka na ramki CAN, zawierające odpowiednie komendy:

- kasowanie pamięci (poza bootloaderem)
- flashowanie
- skok do aplikacji
- test bootloadera - w odpowiedzi wysyła ramkę zwrotną do programatora

Przesyłanie komend odbywa się poprzez wysyłanie ramek z odpowiednimi standardowymi nagłówkami, w których najmłodsze 3 bity wyrażają komendę, zaś górne 8 bitów adres konkretnej STMki. Adres STMki jest whardkodowany w bootloader jako `#define MY_NAME 0xAFU`, gdzie samą wartość można oczywiście arbitralnie zmienić. W celu ułatwienia, repozytorium zawiera skrypt do komunikacji, opisany w dalszej części.  
W przypadku wgrywania programu istotna jest modyfikacja skryptu linkera, tak by przyjmował on za początek pamięci nie typowe w STMach 0x08000000, a uwzględniał obecność bootloadera, zajmującego 8kB pamięci. W tym celu należy w pliku `/STM32F103C8Tx_FLASH.ld` (lub podobnym dla innych kontrolerów) zmodyfikować linię deklarującą rozmiar i położenie pamięci flash,

##### zmienić:

```
MEMORY
{
RAM (xrw)      : ORIGIN = 0x20000000, LENGTH = 20K
FLASH (rx)      : ORIGIN = 0x8000000, LENGTH = 64K
}
```

##### na:

```
MEMORY
{
RAM (xrw)      : ORIGIN = 0x20000000, LENGTH = 20K
FLASH (rx)      : ORIGIN = 0x8002000, LENGTH = 56K
}
```

#### Skrypt do korzystania z bootloadera

---

W repozytorium, w folderze `/can_bootloader/` znajduje sie plik **can_bootloader.py**, zawierający prosty program ułatwiający komunikację z bootloaderem. Aby zapoznać się z opcjami, które umożliwia wystarczy uruchomić go z flagą pomocy:  
```
$ python can_bootloader.py -h 
```

W przypadku, gdy na mikrokontrolerze znajduje się oprócz bootloadera wgrany już program, aby uniemożliwić kontrolerowi skok do programu należy wpierw uruchomić skrypt z flagami `--handshake` oraz `--deviceId ID`, gdzie `ID` to wspomniany już 8-bitowy adres mikrokontrolera `MY_NAME`. Wprowadzi to skrypt w stan oczekiwania na przywitanie ze strony bootloadera. W tym czasie należy zresetować mikrokontroler. Gdy skrypt otrzyma od bootloadera ramkę potwierdzającą jego obecność, odsyła adresowaną do niego ramkę, wymuszającą przejście bootloadera do pętli głównej.  
Oprócz wspomnianych pól, konieczna może okazać się modyfikacja interfejsu oraz kanału CAN wykorzystywanego przez aplikację. Domyślnie jest to *socketcan* oraz *slcan0*. Modyfikacji tej można dokonać przez odpowiednie flagi podczas wywołania programu - `--interface INTERFACE` oraz `--channel CHANNEL`.  
Po wprowadzeniu bootloadera do pętli głównej nie jest potrzebne ponawianie *handshake* aż do resetu kontrolera. W tym czasie można wywołać odpowiednie, wspomniane wyżej operacje poprzez dodanie odpowiednich flag:  
- `--erase`  
- `--flash`  (przy flashowaniu należy wskazać plik .bin za flagą `--file FILE`)
- `--jump_to_app`  
- `--test_loader`  

#### Dalszy rozwój

---

W trakcie pracy nad bootloaderem znacznie zmniejszono wykorzystywaną przezeń pamięć, do nieco ponad 4 kB. Usunięcie inicjalizacji diody oraz RCC - działającego i tak z domyślną dla STM32F1 prędkością 8 MHz - może doprowadzić do redukcji kodu poniżej 4 kB.  
Należy również dodać przesyłanie adresu STMki do programatora w trakcie *handshake*, by uniknąć konfliktów dla większej ilości urządzeń startujących w tym samym momencie.
