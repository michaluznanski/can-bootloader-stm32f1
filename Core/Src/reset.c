#include "reset.h"

extern uint32_t _sidata;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _sitext;
extern uint32_t _stext;
extern uint32_t _eisr;
extern uint32_t _ebss;

void Reset_Handler(void)
{
	uint32_t * rom = &_sitext;
	uint32_t * ram = &_stext;
	uint32_t ram_end = &_ebss;
	while(ram <= (uint32_t*)ram_end) // copy the rest of bootloader to ram
	{
		*ram = *rom;
		ram++;
		rom++;
	}

	// lack of bss section's zeroing, not necessary
	
	main(); // start loader from ram
}