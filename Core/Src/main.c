/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "can.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define MY_NAME 0xAFU // one byte code of our device, chosen arbitrarily, if there are more than one device on CAN bus, they need to have different codes
#define LOADER_NAME 0x88U // computer loader identifier, has to be the same as in python script on the computer

#define FLASH_START_ADDRESS 0x08000000U
#define PAGE_SIZE 1024U
#define BOOTLOADER_PAGES_NUMBER 8U
#define START_PAGE_ADRESS (FLASH_START_ADDRESS + BOOTLOADER_PAGES_NUMBER * PAGE_SIZE)
#define FLASH_PAGES_NUMBER 64U

enum commands
{
	nothing,
	jump,
	erase,
	flash,
	test,
	loader
};
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void jumpToApp(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  //HAL_Init();
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
//   LL_SetSystemCoreClock(8000000);

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN_Init();
  /* USER CODE BEGIN 2 */
	__disable_irq();

	// filter settings, all messages are accepted, if they are useful is checked in while loop
	CAN1->FS1R = 1U;
	CAN1->sFilterRegister[0].FR1 = 0U;
	CAN1->sFilterRegister[0].FR2 = 0U;
	CAN1->FA1R = 1U;
	CAN1->FMR = 0U;

	// blue pill led pin (PC13) is used to indicate that loader is working, not necessary
	LL_GPIO_SetOutputPin(UserLed_GPIO_Port, UserLed_Pin);
	
	HAL_FLASH_Unlock();

	LL_mDelay(2000U);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	HAL_CAN_GetState(&hcan);
	CAN_RxHeaderTypeDef rxHeader;
	CAN_TxHeaderTypeDef txHeader;
	uint8_t data[8];
	uint32_t _; // na śmieci
	uint32_t byteAddress = START_PAGE_ADRESS;

	txHeader.StdId = 2137;
	txHeader.RTR = 1;
	txHeader.IDE = 0;

	CAN1->MCR ^= CAN_MCR_INRQ;

	// let's chceck, if our device is connected or, if not, if there is any app already installed
	txHeader.StdId = LOADER_NAME;
	txHeader.DLC = 0U;
	txHeader.IDE = 0U;
	HAL_CAN_AddTxMessage(&hcan, &txHeader, data, &_);
	
	// waiting for answer
	LL_mDelay(1000U);
	if(((HAL_CAN_GetRxMessage(&hcan, CAN_RX_FIFO0, &rxHeader, data) == HAL_OK) && rxHeader.StdId == ((MY_NAME << (11U - 8U)) | loader)) || (*(uint32_t *)(START_PAGE_ADRESS) != 0x20005000U))
	{
		while (1)
		{
			if(HAL_CAN_GetRxFifoFillLevel(&hcan, CAN_RX_FIFO0) > 0)
			{
				LL_GPIO_TogglePin(UserLed_GPIO_Port, UserLed_Pin); //machamy sobie diódką
				if(HAL_CAN_GetRxMessage(&hcan, CAN_RX_FIFO0, &rxHeader, data) == HAL_OK)
				{
					// if frame identificator contains jump command, than jump to app
					if(rxHeader.StdId == ((MY_NAME << (11U - 8U)) | jump))
					{
						jumpToApp();
					}

					// erase command
					if(rxHeader.StdId == ((MY_NAME << (11U - 8U)) | erase))
					{
						FLASH_EraseInitTypeDef flasherase;
						flasherase.PageAddress = FLASH_START_ADDRESS + BOOTLOADER_PAGES_NUMBER*PAGE_SIZE;
						flasherase.NbPages = FLASH_PAGES_NUMBER - BOOTLOADER_PAGES_NUMBER;
						flasherase.TypeErase = FLASH_TYPEERASE_PAGES;
						if(HAL_FLASHEx_Erase(&flasherase, &_) != HAL_OK)
							Error_Handler();
					}

					// flash command
					if(rxHeader.StdId == ((MY_NAME << (11U - 8U)) | flash))
					{
						if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, byteAddress, *((uint64_t *)data)) == HAL_OK)
						{
							byteAddress += 8U; // 8 bo ładujemy od razu po dwa słowa - tyle mieści się w ramce cana
							// tutaj powinno się jeszcze znaleźć sprawdzanie czy nie piszemy po pamięci
						}
						else
							Error_Handler();
					}

					// test command
					if(rxHeader.StdId == ((MY_NAME << (11U - 8U)) | test))
					{
						HAL_CAN_AddTxMessage(&hcan, &txHeader, data, &_);
					}
					
				}
				else
					HAL_CAN_AddTxMessage(&hcan, &txHeader, data, &_);
			}
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
		}
	}
	else
		jumpToApp();
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_0);
  while(LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_0)
  {
  }
  LL_RCC_HSI_SetCalibTrimming(16);
  LL_RCC_HSI_Enable();

   /* Wait till HSI is ready */
  while(LL_RCC_HSI_IsReady() != 1)
  {

  }
//   LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI_DIV_2, LL_RCC_PLL_MUL_2);
//   LL_RCC_PLL_Enable();

//    /* Wait till PLL is ready */
//   while(LL_RCC_PLL_IsReady() != 1)
//   {

//   }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
//   LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI);

   /* Wait till System clock is ready */
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_HSI)
  {

  }
  LL_SetSystemCoreClock(8000000);

   /* Update the time base */
  if (HAL_InitTick (TICK_INT_PRIORITY) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void jumpToApp(void)
{
	HAL_FLASH_Lock();
	HAL_CAN_DeInit(&hcan);
	RCC->APB2ENR ^= RCC_APB1ENR_CAN1EN; // wyłączamy zegar dla cana
	SCB->VTOR = PAGESIZE * BOOTLOADER_PAGES_NUMBER; // przesuwamy wektor przerwań na początek programu użytkownika
	__enable_irq();
	// void (* app) (void) = *(uint32_t *)(START_PAGE_ADRESS + 4U); // skaczemy do reset handlera
	__set_MSP(*(uint32_t *)(PAGESIZE * BOOTLOADER_PAGES_NUMBER));
	//app();
	((void (*)(void))(*(uint32_t *)(START_PAGE_ADRESS + 4U)))();
	Error_Handler();
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
