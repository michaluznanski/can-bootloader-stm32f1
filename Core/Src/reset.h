#ifndef _reset_h
#define _reset_h

#include "main.h"

void Reset_Handler(void) __attribute__ ((section(".isr"))); // put reset hander in .isr section since it'll be executed from flash memory

#endif