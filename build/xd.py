bytes = file.read(8)
while bytes:
    print(bytes)
    bytes = file.read(8)
    
print('End of work')

import can
import time

class Bootloader:
    def __init__(self, nameOfFile, interface='socketcan', channel='slcan0', busSpeed=125000):
        self.bus = can.interface.Bus(interface=interface, channel=channel, bitrate=busSpeed)
        self.file = open(nameOfFile, 'rb')
        
    def mass_erase(self):
        msg = can.Message(arbitration_id=2)
        self.bus.send(msg)
        time.sleep(5.) # to dla pewności, by zdążył wszystko wykasować
        
    def test_loader(self):
        msg = can.Message(arbitration_id=4)
        self.bus.send(msg) # powinien mrugnąć diodą z półsekundowym opóźnieniem
        time.sleep(0.5)
        
    def flashIt(self):
        bytes = self.file.read(8)
        while bytes:
            msg = can.Message(arbitration_id=3, data=bytes)
            self.bus.send(msg)
            print('Flashing bytes: {}'.format(bytes))
            time.sleep(0.001) # potem można wywalić
            bytes = self.file.read(8)
            
    def jump_to_app(self):
        msg = can.Message(arbitration_id=1)
        self.bus.send(msg)